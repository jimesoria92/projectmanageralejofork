﻿using EscuelaNet.Dominio.Proyectos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Proyectos.Web.Models
{
    public class ProyectoIndexModel
    {
        public string Titulo { get; set; }
        public List<Proyecto> Proyectos { get; set; }
        public int Linea { get; internal set; }
    }
}