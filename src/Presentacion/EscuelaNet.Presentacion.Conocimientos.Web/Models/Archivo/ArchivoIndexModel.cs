﻿using EscuelaNet.Dominio.Conocimientos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Archivos.Web.Models
{
    public class ArchivoIndexModel
    {
        public string Nombre { get; set; }
        public IList<Archivo> Archivos { get; set; }
    }
}