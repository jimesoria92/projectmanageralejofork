﻿using EscuelaNet.Dominio.Conocimientos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Conocimientos.Web.Models
{
    public class AsesorConocimientoModel
    {
        public Conocimiento Conocimiento { get; set; }
        public List<Conocimiento> Conocimientos { get; set; }
        public Asesor Asesor { get; set; }
        public List<Asesor> Asesores { get; set; }
    }
}