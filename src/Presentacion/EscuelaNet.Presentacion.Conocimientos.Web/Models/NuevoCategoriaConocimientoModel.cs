﻿using EscuelaNet.Dominio.Conocimientos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Conocimientos.Web.Models
{
    public class NuevoCategoriaConocimientoModel
    {
        public int IDCategoria { get; set; }
        public int IDConocimiento { get; set; }
        public string NombreConocimiento { get; set; }
        public string NombreCategoria { get; set; }
        public List<Conocimiento> Conocimientos { get; set; }
    }
}