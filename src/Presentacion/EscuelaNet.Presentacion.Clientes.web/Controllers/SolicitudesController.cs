﻿using EscuelaNet.Aplicacion.Clientes.QueryServices;
using EscuelaNet.Dominio.Clientes;
using EscuelaNet.Presentacion.Clientes.Web.Models;
using EsculaNet.Infraestructura.Clientes.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Clientes.Web.Controllers
{
    public class SolicitudesController : Controller
    {
        private ISolicitudRepository _solicitudesRepositorio;
        private IClienteRepository _clientesRepositorio;
        private ISolicitudesQuery _solicitudesQuery;
        private IUnidadesQuery _unidadesQuery;

        public SolicitudesController(ISolicitudRepository solicitudesRepositorio, 
                IClienteRepository clientesRepositorio,
                IUnidadesQuery unidadesQuery,
                ISolicitudesQuery solicitudesQuery)
        {
            _solicitudesRepositorio = solicitudesRepositorio;
            _clientesRepositorio = clientesRepositorio;
            _solicitudesQuery = solicitudesQuery;
            _unidadesQuery = unidadesQuery;
        }

        public ActionResult Index()
        {
                var solicitudes = _solicitudesQuery.ListSolicitud();                      
                var model = new SolicitudesIndexModel()
                {
                    Titulo = "Solicitudes",
                    Solicitudes = solicitudes,                    
                };

                return View(model);                          
        }

        public ActionResult New()
        {                
            var model = new NuevaSolicitudModel()
            {
                Title = "Nueva Solicitud ",
            };
            return View(model);                        
        }

        [HttpPost]
        public ActionResult New(NuevaSolicitudModel model)
        {
            if (!string.IsNullOrEmpty(model.Titulo))
            {
                try
                {
                    var solicitud = new Solicitud(model.Titulo, model.Descripcion);
                    _solicitudesRepositorio.Add(solicitud);
                    _solicitudesRepositorio.UnitOfWork.SaveChanges();

                    TempData["success"] = "Solicitud creada";
                    
                    return RedirectToAction("Index");       
                        
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }
            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }

        public ActionResult Edit(int id)
        {
            var solicitud = _solicitudesQuery.GetSolicitud(id);
            if (solicitud==null)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("Index");
            }
            else
            {    
                var model = new NuevaSolicitudModel()
                {
                    Title = "Editar la solicitud " + solicitud.Titulo,
                    IdSolicitud = id,
                    Titulo = solicitud.Titulo,
                    Descripcion = solicitud.Descripcion,
                    Estado = solicitud.Estado
                };

                return View(model);
            }
            
        }

        [HttpPost]
        public ActionResult Edit(NuevaSolicitudModel model)
        {
            if (!string.IsNullOrEmpty(model.Titulo))
            {
                try
                {
                    var solicitud = _solicitudesRepositorio.GetSolicitud(model.IdSolicitud);
                    solicitud.Titulo = model.Titulo;
                    solicitud.Descripcion = model.Descripcion;
                    solicitud.CambiarEstado(model.Estado);

                    _solicitudesRepositorio.Update(solicitud);
                    _solicitudesRepositorio.UnitOfWork.SaveChanges();
                    TempData["success"] = "Solicitud editada";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }

        public ActionResult Delete(int id)
        {
            var solicitud = _solicitudesQuery.GetSolicitud(id);

            if (solicitud==null)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("Index");
            }
            else
            {             
                var model = new NuevaSolicitudModel()
                {
                    IdSolicitud = id,
                    Titulo = solicitud.Titulo,
                    Descripcion = solicitud.Descripcion,
                    Estado = solicitud.Estado
                };

                return View(model);
            }               
        }

        [HttpPost]
        public ActionResult Delete(NuevaSolicitudModel model)
        {
            try
            {
                var solicitud = _solicitudesRepositorio.GetSolicitud(model.IdSolicitud);
                _solicitudesRepositorio.Delete(solicitud);
                _solicitudesRepositorio.UnitOfWork.SaveChanges();
                TempData["success"] = "Solicitud borrada";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }

        public ActionResult Unidades(int id)
        {

            var solicitud = _solicitudesQuery.GetSolicitud(id);
            var unidades = _solicitudesQuery.ListUnidadesDeSolicitud(id);
            var model = new UnidadSolicitudModel()
            {
                Solicitud = solicitud,
                Unidades = unidades
            };

            return View(model);

        }

        public ActionResult UnlinkUnidad(int id, int solicitud)
        {            
            var solicitudBuscada = _solicitudesQuery.GetSolicitud(solicitud);
            var unidadBuscada = _solicitudesQuery.FindUnidadEnSolicitud(id, solicitud);

            if (unidadBuscada != null && solicitudBuscada != null)
            {
                var model = new NuevaUnidadSolicitudModel()
                {
                    IDSolicitud = solicitud,
                    IDUnidad = id,
                    TituloSolicitud = solicitudBuscada.Titulo,
                    RazonSocialUnidad = unidadBuscada.RazonSocial
                };
                return View(model);

            }
            else
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("../Solicitudes/index");
            }
        }

        [HttpPost]
        public ActionResult UnlinkUnidad(NuevaUnidadSolicitudModel model)
        {
            try
            {
                var solicitudBuscada = _solicitudesRepositorio.GetSolicitud(model.IDSolicitud);
                var unidadBuscada = _clientesRepositorio.GetUnidadDeNegocio(model.IDUnidad);
                unidadBuscada.PullSolicitud(solicitudBuscada);

                _solicitudesRepositorio.Update(solicitudBuscada);
                _solicitudesRepositorio.UnitOfWork.SaveChanges();

                TempData["success"] = "Solicitud desvinculada";
                return RedirectToAction("../Solicitudes/Unidades/" + model.IDSolicitud);
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }

        public ActionResult LinkUnidad(int id)
        {
            var solicitud = _solicitudesQuery.GetSolicitud(id);
            var unidades = _unidadesQuery.ListTodasUnidades();
            
            var model = new NuevaUnidadSolicitudModel()
            {
                IDSolicitud = id,
                TituloSolicitud = solicitud.Titulo,
                Unidades = unidades
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult LinkUnidad(NuevaUnidadSolicitudModel model)
        {
            try
            {
                var solicitudBuscada = _solicitudesRepositorio.GetSolicitud(model.IDSolicitud);
                var unidadBuscada = _clientesRepositorio.GetUnidadDeNegocio(model.IDUnidad);

                var bandera = true;
                var bandera2 = true;

                foreach (var unidadLista in solicitudBuscada.UnidadesDeNegocio)
                {                    
                    if (unidadLista.IDCliente != unidadBuscada.Cliente.ID )
                    {
                        bandera = false;
                    }
                    if (unidadLista.ID == model.IDUnidad)
                    {
                        bandera2 = false;
                    }
                }
                if (bandera && bandera2)
                {
                    unidadBuscada.AgregarSolicitud(solicitudBuscada);
                    _clientesRepositorio.Update(unidadBuscada.Cliente);
                    _clientesRepositorio.UnitOfWork.SaveChanges();

                    TempData["success"] = "Solicitud vinculada";
                    return RedirectToAction("../Solicitudes/Unidades/" + model.IDSolicitud);
                }
                else if (bandera2)
                {
                    TempData["error"] = "Solicitud vinculada a una unidad de otro cliente";
                    return RedirectToAction("../Solicitudes/Unidades/" + model.IDSolicitud);
                }
                else
                {
                    TempData["error"] = "Solicitud ya vinculada a esta unidad";
                    return RedirectToAction("../Solicitudes/Unidades/" + model.IDSolicitud);
                }                
            }
            catch (Exception ex)
            {                
                TempData["error"] = ex.Message;
                return RedirectToAction("../Solicitudes/Unidades/" + model.IDSolicitud);
            }

        }


    }
}