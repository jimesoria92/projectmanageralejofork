﻿using EscuelaNet.Aplicacion.Clientes.QueryServices;
using EscuelaNet.Dominio.Clientes;
using EscuelaNet.Presentacion.Clientes.Web.Models;
using EsculaNet.Infraestructura.Clientes;
using EsculaNet.Infraestructura.Clientes.Repositorios;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Clientes.Web.Controllers
{
    public class DireccionesController : Controller
    {
        private IClienteRepository _clienteRepositorio;
        private IDireccionesQuery _direccionesQuery;
        private IUnidadesQuery _unidadesQuery;


        public DireccionesController(IClienteRepository clienteRepositorio, 
                IDireccionesQuery direccionesQuery,
                IUnidadesQuery unidadesQuery)
        {
            _clienteRepositorio = clienteRepositorio;
            _direccionesQuery = direccionesQuery;
            _unidadesQuery = unidadesQuery;
        }

        public ActionResult Index(int id)
        {       
            var direcciones = _direccionesQuery.ListDirecciones(id);

            if (direcciones.Count!=0)
            {
                var model = new DireccionesIndexModel()
                {
                    Titulo = "Direcciones de la Unidad '" + direcciones[0].RazonSocialUnidad + "'",
                    Direcciones = direcciones,
                    IdUnidad = id,
                    IdCliente = direcciones[0].IdCliente
                };

                return View(model);
            }
            else
            {
                var unidad = _unidadesQuery.GetUnidadDeNegocio(id);
                TempData["error"] = "Unidad sin direcciones";
                return RedirectToAction("../Unidades/Index/"+unidad.IdCliente);
            }
        }

        public ActionResult New(int id)
        {
            var unidad = _unidadesQuery.GetUnidadDeNegocio(id);
            var model = new NuevaDireccionModel()
            {
                Titulo = "Nueva Direccion para la  Unidad '"+unidad.RazonSocial+"'",
                IdUnidad = id
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult New(NuevaDireccionModel model)
        {
            if (!string.IsNullOrEmpty(model.Domicilio))
            {
                try
                {                    
                    var domicilio = model.Domicilio;
                    var localidad = model.Localidad;
                    var provincia = model.Provincia;
                    var pais = model.Pais;

                    var unidadDeNegocio = _clienteRepositorio.GetUnidadDeNegocio(model.IdUnidad);
                    var direccion = new Direccion(domicilio,localidad,provincia,pais);
                    
                    unidadDeNegocio.AgregarDireccion(direccion);
                    _clienteRepositorio.Update(unidadDeNegocio.Cliente);
                    _clienteRepositorio.UnitOfWork.SaveChanges();
                    TempData["success"] = "Dirección creada";
                    return RedirectToAction("Index/"+model.IdUnidad);

                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }
        
        public ActionResult Edit(int id)
        {
            var direccion = _direccionesQuery.GetDireccion(id);
            if (direccion==null)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("../Clientes/Index");
            }
            else
            {                
                var unidad = _unidadesQuery.GetUnidadDeNegocio(direccion.IdUnidadDeNegocio);
                var model = new NuevaDireccionModel()
                {
                    Titulo="Editar una Direccion  de la Unidad'"+unidad.RazonSocial+"' del Cliente '"+ unidad.RazonSocialCliente+"'",                    
                    IdUnidad = direccion.IdUnidadDeNegocio,
                    IdDireccion = id,
                    Domicilio = direccion.Domicilio,
                    Localidad = direccion.Localidad,
                    Provincia = direccion.Provincia,
                    Pais = direccion.Pais

                };

                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Edit(NuevaDireccionModel model)
        {
            if (!string.IsNullOrEmpty(model.Domicilio))
            {
                try
                {                    
                    var unidadDeNegocio = _clienteRepositorio.GetUnidadDeNegocio(model.IdUnidad);

                    unidadDeNegocio.Direcciones.Where(d => d.ID == model.IdDireccion).First().Domicilio = model.Domicilio;
                    unidadDeNegocio.Direcciones.Where(d => d.ID == model.IdDireccion).First().Localidad = model.Localidad;
                    unidadDeNegocio.Direcciones.Where(d => d.ID == model.IdDireccion).First().Provincia = model.Provincia;
                    unidadDeNegocio.Direcciones.Where(d => d.ID == model.IdDireccion).First().Pais = model.Pais;
                                        
                    _clienteRepositorio.Update(unidadDeNegocio.Cliente);
                    _clienteRepositorio.UnitOfWork.SaveChanges();
                    TempData["success"] = "Direccion editada";
                    return RedirectToAction("Index/"+model.IdUnidad);
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }

        public ActionResult Delete(int id)
        {
            var direccion = _direccionesQuery.GetDireccion(id);

            if (direccion==null)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("../Clientes/Index");
            }
            else
            {
                var unidad = _unidadesQuery.GetUnidadDeNegocio(direccion.IdUnidadDeNegocio);
                var model = new NuevaDireccionModel()
                {
                    Titulo = "Borrar una Direccion  de la Unidad '" + unidad.RazonSocial + "' del Cliente '" + unidad.RazonSocialCliente + "'",                   
                    IdUnidad = direccion.IdUnidadDeNegocio,
                    IdDireccion = id,
                    Domicilio = direccion.Domicilio,
                    Localidad = direccion.Localidad,
                    Provincia = direccion.Provincia,
                    Pais = direccion.Pais

                };
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Delete(NuevaDireccionModel model)
        {

            try
            {
                var direccion = _clienteRepositorio.GetDireccion(model.IdDireccion);
                _clienteRepositorio.DeleteDireccion(direccion);
                _clienteRepositorio.UnitOfWork.SaveChanges();
                TempData["success"] = "Direccion borrada";
                return RedirectToAction("Index/" + model.IdUnidad);
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }

        }

    }
}