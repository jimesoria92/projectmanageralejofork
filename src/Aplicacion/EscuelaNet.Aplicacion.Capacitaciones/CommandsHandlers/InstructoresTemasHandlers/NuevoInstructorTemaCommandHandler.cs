﻿using EscuelaNet.Aplicacion.Capacitaciones.Commands.InstructorTemaCommand;
using EscuelaNet.Aplicacion.Capacitaciones.Responds;
using EscuelaNet.Dominio.Capacitaciones;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Capacitaciones.CommandsHandlers.InstructoresHandlers
{
    public class NuevoInstructorTemaCommandHandler : IRequestHandler<NuevoInstructorTemaCommand, CommandRespond>
    {
        private IInstructorRepository _instructorRepository;
        private ITemaRepository _temaRepository;
        public NuevoInstructorTemaCommandHandler(IInstructorRepository instructorRepository, ITemaRepository temaRepository)
        {
            _instructorRepository = instructorRepository;
            _temaRepository = temaRepository;
        }

        public Task<CommandRespond> Handle(NuevoInstructorTemaCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();

            try
            {
                var instructorBuscado = _instructorRepository.GetInstructor(request.IDInstructor);
                var temabuscado = _temaRepository.GetTema(request.IDTema);

                instructorBuscado.pushTema(temabuscado);
                _instructorRepository.Update(instructorBuscado);
                _instructorRepository.UnitOfWork.SaveChanges();

                responde.Succes = true;
                return Task.FromResult(responde);
            }
            catch (Exception ex)
            {
                responde.Succes = false;
                responde.Error = "La Capacidad debe ser mayor a 0";
                return Task.FromResult(responde);
            }


        }
    }
}
