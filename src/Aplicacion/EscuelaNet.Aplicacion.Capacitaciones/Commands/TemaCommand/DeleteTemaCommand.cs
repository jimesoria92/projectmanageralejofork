﻿using EscuelaNet.Aplicacion.Capacitaciones.Responds;
using EscuelaNet.Dominio.Capacitaciones;
using MediatR;

namespace EscuelaNet.Aplicacion.Capacitaciones.Commands.TemaCommand
{
    public class DeleteTemaCommand : IRequest<CommandRespond>
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public NivelTema Nivel { get; set; }
    }
}
