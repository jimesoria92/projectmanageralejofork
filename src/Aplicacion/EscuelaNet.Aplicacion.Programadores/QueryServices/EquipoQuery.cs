﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using EscuelaNet.Aplicacion.Programadores.QueryModels;

namespace EscuelaNet.Aplicacion.Programadores.QueryServices
{
    public class EquipoQuery : IEquipoQuery
    {
        //COMO SE VINCULA EL _CONNECTIONSTRING
        //EN LA QUERY DE EQUIPO TENGO QUE HACER UN JOIN CON SKILL?
        private string _connectionString;
        public EquipoQuery(string connectionString)
        {
            _connectionString = connectionString;
        }
        public EquipoQueryModel GetEquipo(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<EquipoQueryModel>(
                    @"
                    SELECT e.IdEquipo as Id, e.Nombre as Nombre, e.Pais as Pais, e.HusoHorario as HusoHorario,
                    e.CantidadProgramadores as CantidadProgramadores, e.HorasDisponibleProgramadores as HorasDisponibleProgramadores
                    FROM Equipo as e
                    WHERE e.IdEquipo = @id
                    ", new { id = id }
                    ).FirstOrDefault();
            }
        }

        public List<EquipoQueryModel> ListEquipo()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<EquipoQueryModel>(
                    @"
                    SELECT e.IdEquipo as Id, e.Nombre as Nombre, e.Pais as Pais, e.HusoHorario as HusoHorario,
                    e.CantidadProgramadores as CantidadProgramadores, e.HorasDisponibleProgramadores as HorasDisponibleProgramadores,
                    es.Total as Total
                    FROM Equipo as e
					LEFT JOIN (
					    SELECT Equipo_ID, COUNT(Equipo_ID) as Total
                        FROM EquipoSkill 
                        GROUP BY Equipo_ID
					) as es ON e.IdEquipo = es.Equipo_ID
                    ORDER BY Total DESC
                    "
                    ).ToList();
            }
        }
    }
}
