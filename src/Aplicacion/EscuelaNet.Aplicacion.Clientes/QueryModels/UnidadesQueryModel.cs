﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Clientes.QueryModels
{
    public class UnidadesQueryModel
    {
        public int IdCliente { get; set; }

        public string RazonSocialCliente { get; set; }

        public int ID { get; set; }

        public string RazonSocial { get; set; }

        public string ResponsableDeUnidad { get; set; }

        public string Cuit { get; set; }

        public string EmailResponsable { get; set; }

        public string TelefonoResponsable { get; set; }



    }
}
