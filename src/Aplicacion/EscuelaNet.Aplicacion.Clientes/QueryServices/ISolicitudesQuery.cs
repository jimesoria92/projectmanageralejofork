﻿using EscuelaNet.Aplicacion.Clientes.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Clientes.QueryServices
{
    public interface ISolicitudesQuery
    {

        SolicitudesQueryModel GetSolicitud(int id);

        List<SolicitudesQueryModel> ListSolicitud();

        List<UnidadesQueryModel> ListUnidadesDeSolicitud(int id);

        UnidadesQueryModel FindUnidadEnSolicitud(int id, int solicitud);
    }
}
