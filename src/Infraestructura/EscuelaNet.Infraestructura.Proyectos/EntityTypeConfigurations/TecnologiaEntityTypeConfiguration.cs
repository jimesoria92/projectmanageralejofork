﻿using EscuelaNet.Dominio.Proyectos;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Infraestructura.Proyectos.EntityTypeConfigurations
{
    public class TecnologiaEntityTypeConfiguration : EntityTypeConfiguration<Tecnologias>
    {
        public TecnologiaEntityTypeConfiguration()
        {
            this.ToTable("Tecnologias");
            this.HasKey<int>(tecno => tecno.ID);
            this.Property(tecno => tecno.ID)
                .HasColumnName("IDTecnologia");
            this.Property(tecno => tecno.nombre)
                .IsRequired();

        }

    }
}
