﻿using EscuelaNet.Dominio.Clientes;
using System.Data.Entity.ModelConfiguration;

namespace EsculaNet.Infraestructura.Clientes.EntityTypeConfigurations
{
    public class ClientesEntityTypeConfiguration : 
        EntityTypeConfiguration<Cliente>
    {
        public ClientesEntityTypeConfiguration()
        {
            this.ToTable("Clientes");
            this.HasKey<int>(c => c.ID);
            this.Property(c => c.ID)
                .HasColumnName("IDCliente");
            this.Property(c => c.RazonSocial)
                .IsRequired();
            this.Property(c => c.Email)
                .IsRequired();
            this.Property(c => c.Categoria)
                .IsRequired();
            this.HasMany<UnidadDeNegocio>(c => c.Unidades)
                .WithRequired(un => un.Cliente)
                .HasForeignKey<int>(un => un.IDCliente);
                
        }
    }
}