﻿using EscuelaNet.Dominio.Conocimientos;
using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Infraestructura.Conocimientos.Repositorios
{
    public class CategoriaRepositorio : ICategoriaRepository
    {
        private CategoriaContext _context = new CategoriaContext();
        public IUnitOfWork UnitOfWork => _context;
        public Categoria Add(Categoria categoria)
        {
            _context.Categorias.Add(categoria);
            return categoria;
        }

        public Asesor AddAsesor(Conocimiento conocimiento, Asesor asesor)
        {
            _context.Entry(asesor).State = EntityState.Unchanged;
            _context.Entry(conocimiento.Categoria).State = EntityState.Unchanged;

            try
            {
                conocimiento.AgregarAsesor(asesor);
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return asesor;
        }

        public void Delete(Categoria categoria)
        {
            _context.Categorias.Remove(categoria);
        }

        public void DeleteAsesor(Conocimiento conocimiento, Asesor asesor)
        {
            _context.Entry(asesor).State = EntityState.Unchanged;
            _context.Entry(conocimiento.Categoria).State = EntityState.Unchanged;
            try
            {
                conocimiento.AgregarAsesor(asesor);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteConocimiento(Conocimiento conocimiento)
        {
            _context.Conocimientos.Remove(conocimiento);
        }

        public Categoria GetCategoria(int id)
        {
            var categoria = _context.Categorias.Find(id);
            if (categoria != null)
            {
                _context.Entry(categoria).Collection(c => c.Conocimientos).Load();
            }
            return categoria;
        }

        public Conocimiento GetConocimieto(int id)
        {
            var conocimiento = _context.Conocimientos.Find(id);
            if (conocimiento != null)
            {
                _context.Entry(conocimiento).Reference(con => con.Categoria).Load();
                _context.Entry(conocimiento).Reference(con => con.Asesores).Load();
            }
            return conocimiento;
        }

        public List<Categoria> ListCategoria()
        {
            return _context.Categorias.ToList();
        }

        public void Update(Categoria categoria)
        {
            _context.Entry(categoria).State = EntityState.Modified; //Stade es el estado en el que esta
        }

    }
}
