﻿using System;
using System.Collections.Generic;
using EscuelaNet.Dominio.Proyectos;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EscuelaNet.Dominio.Capacitaciones.Test
{
    [TestClass]
    public class UnitTestCapacitaciones
    {
        [TestMethod]
        public void PROBAR_CAPACITACION()
        {
            var capa = new Capacitacion();
            Assert.AreEqual(0, capa.GetDuracion());
            Action action = () =>
            {
                capa.SetDuracion(-10);

            };
            Assert.ThrowsException<ExcepcionDeProyectos>(action);
        }

        [TestMethod]
        public void ADD_ALUMNO()
        {
            var capa = new Capacitacion();

            var edad = DateTime.Now;

            var alumno = new Alumno("Matias","Juarez","1235",edad);

            capa.PushAlumno(alumno);
            Assert.AreEqual("Matias", capa.Alumnos[0].Nombre);

        }

        [TestMethod]
        public void ADD_TEMA()
        {
            var capa = new Capacitacion();

            var tema = new Tema("MVC",NivelTema.Avanzado);
         
            capa.AddTema(tema);

            Assert.AreEqual("MVC", capa.Temas[0].Nombre);

        }


        [TestMethod]
        public void ADD_LUGAR()
        {

            var lugar = new Lugar(1, "Las Piedras", "231", "", "", "San Miguel de Tucuman", "Tucuman", "Argentina");

        }
    }
}
